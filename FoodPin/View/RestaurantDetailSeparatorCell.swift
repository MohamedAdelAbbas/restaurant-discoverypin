//
//  RestaurantDetailSeparatorCell.swift
//  FoodPin
//
//  Created by Mohamed Adel on 6/15/20.
//  Copyright © 2020 Mohamed Adel. All rights reserved.
//

import UIKit

class RestaurantDetailSeparatorCell: UITableViewCell {

    @IBOutlet var titleLabel: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
